# Obsidian

## My Obsidian tutorial

- [Obsidian Series 0 - Why Obsidian](https://blog.chivier.site/2023/03/04/2023/Obsidian-Series-0---Why-Obsidian/)
- [Obsidian Series 1 - From Markdown To Excellent Notes](https://blog.chivier.site/2023/03/04/2023/Obsidian-Series-1---From-Markdown-To-Excellent-Notes/)
- [Obsidian Series 2 - Basic Settings](https://blog.chivier.site/2023/03/06/2023/Obsidian-Series-2---Basic-Settings/)
- [Obsidian Series 3 - Appearance](https://blog.chivier.site/2023/03/09/2023/Obsidian-Series-3---Appearance/)
- [Obsidian Tricks 1 - Sync](https://blog.chivier.site/2023/03/10/2023/Obsidian-Tricks-1---Sync/)
- [Obsidian Tricks 2 - Multi-Column](https://blog.chivier.site/2023/03/10/2023/Obsidian-Tricks-2---Multi-Column/)
- [Obsidian Tricks 3 - Clean Useless Files](https://blog.chivier.site/2023/03/14/2023/Obsidian-Tricks-3---Clean-Useless-Files/)
