# AI Tools

## ChatGPT Related

**Prompts sharing**

- [OpenGPT](https://open-gpt.app/en)
- [Open Prompt](https://openprompt.co/)
- [awesome-chatgpt-prompts-zh (github)](https://github.com/PlexPt/awesome-chatgpt-prompts-zh)

**Prompt editing**

- [OpenPromptStudio](https://github.com/Moonvy/OpenPromptStudio)
  - Based on Notion.


**Better desktop UI**

- [chatbox (github)](https://github.com/Bin-Huang/chatbox/)
  - The Ultimate Copilot on Your Desktop. Chatbox is a desktop app for GPT-4 / GPT-3.5.

- [BingGPT (github)](https://github.com/dice2o/BingGPT)
  - Desktop application of new Bing's AI-powered chat (Windows, macOS and Linux).


**PDF reader**

- [ChatPDF](https://www.chatpdf.com/)
  - Chat with any PDF!

- [ChatFiles (github)](https://github.com/guangzhengli/ChatFiles)
  - Have a conversation with files.

- [researchgpt (github)](https://github.com/mukulpatnaik/researchgpt)
  - An open-source LLM based research assistant that allows you to have a conversation with a research paper.


**Researching tools**

- [Elicit: The AI Research Assistant](https://elicit.org/)
  - Help me list out related papers.
  - Similar to [Connected Papers](https://www.connectedpapers.com/)

**Others**

- [诗片](https://shipian.danieljia.work/)
  - Write poem for a picture.
- [Free ChatGPT Site List (github)](https://github.com/xx025/carrot)


## AI Painting & Image Generation

- [Civitai](https://civitai.com/)
  - Stable Diffusion models, embeddings, hypernetworks and more.

- [Microsoft Designer](https://designer.microsoft.com/)
  - Stunning designs in a flash


# Others

- [Futurepedia](https://www.futurepedia.io/)
  - The Largest AI Tools Directory.

