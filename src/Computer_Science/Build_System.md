# Build System

# What is the build system

[List of build automation software - Wikipedia](https://en.wikipedia.org/wiki/List_of_build_automation_software)

[A Brief Introduction to Build Systems | by Chuan Zhang | Medium](https://medium.com/swlh/a-brief-introduction-to-build-systems-1e45cb1cf667)

## Design a new build system

[Bake build system - Nim Days (xmonader.github.io)](https://xmonader.github.io/nimdays/day11_buildsystem.html)

[Designing a Common Build System](https://alesnosek.com/blog/2018/05/03/designing-a-common-build-system/)
