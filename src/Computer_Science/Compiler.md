# Compiler

## Compiler General Knowledge

[Compiler - Wikipedia](https://en.wikipedia.org/wiki/Compiler) - Wikipedia gives a general knowledge of compilers.

[Engineering a Compiler - Google Books](https://www.google.com.hk/books/edition/_/WxTozgEACAAJ?hl=en&sa=X&ved=2ahUKEwipzcef7cz-AhVbklYBHVgTD_kQ7_IDegQIJxAD) - The best compiler book I have ever read.

[Compilers | Course | Stanford Online](https://online.stanford.edu/courses/soe-ycscs1-compilers)



## Compiler Tools

[Compiler Explorer (godbolt.org)](https://gcc.godbolt.org/) - Best online compiler explorer.

