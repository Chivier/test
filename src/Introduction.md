# Introduction

[TOC]

This is my personal knowledge gardon.

## Why build a wiki?

As I prepare for the next stage of my PhD studies, I recognize the importance of having an honest conversation with myself. While I have found fulfilment in helping others and contributing to various projects, I have come to realize that my research skills have not developed as much as I would have liked. As a result, I have been struggling with the decision to pursue a stable job or to follow my dream of becoming a researcher. After careful consideration, I have decided that working as a researcher would provide me with the intellectual challenge and curiosity that I crave.

To achieve this goal, I plan to take a break from my studies and review my previous academic knowledge. On the one hand, I want to review my bachelor's and postgraduate's knowledge and make some notes. This helps me make clear what I can do. On the other hand, I want to make a lucid view of my border and make a more deterministic target. During this time, I aim to develop a clear understanding of my strengths, weaknesses, and career goals. Additionally, I hope to support my brother and friends who are pursuing computer science by organizing and sharing my study resources to accelerate their learning. 

In a rapidly changing world, I believe that it is crucial to continuously learn, remain energetic, and appreciate the beauty of life. Through my commitment to personal growth and innovation, I am confident that I can make meaningful contributions to the field of research and beyond.

## Arrangement

In order to make my knowledge base more accessible, I have organized the content according to the subject matter. Although my expertise in computer science is particularly strong, I have covered a wide range of topics in this field, from basic theory to advanced development skills. The subsequent section of my knowledge base focuses on natural sciences such as physics and mathematics, both of which I find particularly engaging.

Finally, I have included a selection of other useful skills and helpful tips in the last part of the knowledge base. By categorizing my insights in this manner, I hope to provide readers with a clear and simple path to gaining greater understanding on a variety of topics.

## Tools in this projects

### mdbook

mdbook is a rust project that works like a static website generator, but more than a generator. It also provides a service for hosting and building static websites. For more information, see [mdbook](https://rust-lang.github.io/mdBook).

### slmd

All of the topics in the table of content are flexible and I often add new topics here. To make them more organizable, I use a sort tool called [slmd](https://github.com/lqez/slmd), which helps me sorts all the unordered lists of topics.

### mdbook-toc

[mdbook-toc](https://github.com/badboy/mdbook-toc) helps me generate 'Table of Content' on the heading of some long pages. This might also be helpful to get a quick overview of the knowledge in a certain long wiki page.

### mdbook-admonish

[mdbook-admonish](https://github.com/tommilligan/mdbook-admonish) creates blocks of text in an interesting and good-looking style, such as:

```admonish example
This is an example
```

```admonish note
This is a note
```

```admonish warning
This is a warning
```

